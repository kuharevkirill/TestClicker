using UnityEngine;
using UnityEngine.EventSystems;

namespace ClickerGame
{
    public abstract class BaseEnemy : MonoBehaviour, IEnemy
    {
        public EnemyData EnemyData;

        [SerializeField]
        private ParticleSystem HitParticles;

        [SerializeField]
        private GameObject DeathParticles; 

        public Vector3 Position
        {
            get { return transform.position; }
            set { transform.position = value; }
        }
        protected Quaternion Rotation
        {
            get { return transform.rotation; }
            set { transform.rotation = value; }
        }

        protected Rigidbody Rigidbody;

        protected int MaxHP;
        protected int CurrentHP;

        private IEnemyRepository EnemyRepository;
        private IEnemySpawner EnemySpawner;

        public void OnPointerClick(PointerEventData eventData)
        {
            HandleClick(1);
        }

        public void HandleClick(int damage)
        {
            if (CurrentHP > 0)
            {
                HitParticles.Play();
            }

            CurrentHP -= damage;
            if (CurrentHP <= 0)
            {
                var particles = Instantiate(DeathParticles);
                particles.transform.position = transform.position;
                Die();
            }
        }

        public void SetParent(Transform parent)
        {
            transform.SetParent(parent, false);
        }

        protected void Die()
        {
            gameObject.SetActive(false);
        }

        protected void Initiate()
        {
            // Difficulty progress calculations ::
            // evaluation of hp growth and modifying MaxHP
            int value = EnemySpawner.GetSpawnedEnemiesCount();
            int extraHP = Mathf.FloorToInt(value / EnemyData.HPGrowthRate);
            MaxHP = EnemyData.BaseMaxHP + extraHP;
            CurrentHP = MaxHP;
            //Debug.Log($"MaxHP : {MaxHP}");
        }

        protected float RandomFloat(float min, float max)
        {
            return UnityEngine.Random.Range(min, max);
        }

        private void Awake()
        {
            EnemyRepository = CompositionRoot.GetEnemyRepository();
            EnemySpawner = CompositionRoot.GetEnemySpawner();
            Rigidbody = GetComponent<Rigidbody>();
        }

        protected void OnEnable()
        {
            EnemyRepository.Register(this);
            Initiate();
        }

        protected void OnDisable()
        {
            EnemyRepository.Unregister(this);
        }

        protected virtual void FixedUpdate()
        {

        }

    }
}