using UnityEngine;

namespace ClickerGame
{
    public class Delayer : BaseBooster
    {
        [SerializeField]
        private float ExtraCooldown = 2f;

        private IEnemySpawner EnemySpawner;

        protected override void Awake()
        {
            base.Awake();
            EnemySpawner = CompositionRoot.GetEnemySpawner();
        }

        protected override void Collect()
        {
            base.Collect();
            EnemySpawner.AddCooldownTime(ExtraCooldown);
        }
    }
}