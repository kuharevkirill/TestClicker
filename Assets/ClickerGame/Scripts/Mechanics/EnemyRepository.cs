using System;
using System.Collections.Generic;
using UnityEngine;

namespace ClickerGame
{
    public class EnemyRepository : MonoBehaviour, IEnemyRepository
    {
        public event Action<int> MonsterAdded = value => { };
        public event Action<int, IEnemy> MonsterKilled = (value, enemy) => { };

        public int MonsterCount
        {
            get { return EnemyList.Count; }
        }

        private List<IEnemy> EnemyList = new List<IEnemy>();

        public List<IEnemy> GetEnemyList()
        {
            return EnemyList;
        }

        public void Register(IEnemy enemy)
        {
            EnemyList.Add(enemy);
            MonsterAdded(MonsterCount);
        }

        public void Unregister(IEnemy enemy)
        {
            EnemyList.Remove(enemy);
            MonsterKilled(MonsterCount, enemy);
        }
    }
}

