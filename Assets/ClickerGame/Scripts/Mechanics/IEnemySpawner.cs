using System;

namespace ClickerGame
{
    public interface IEnemySpawner
    {
        event Action<float> TimerProgress;
        void AddCooldownTime(float value);
        int GetSpawnedEnemiesCount();
    }
}