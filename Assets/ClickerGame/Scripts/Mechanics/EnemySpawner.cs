using System;
using UnityEngine;

namespace ClickerGame
{
    public class EnemySpawner : MonoBehaviour, IEnemySpawner
    {
        public event Action<float> TimerProgress = value => { };

        [SerializeField]
        private float SpawnTime = 2f;

        private IResourceManager ResourceManager;

        private float Timer = 0f;
        private float TimeToWait = 0f;
        private int SpawnedEnemiesCount = 0;

        public void AddCooldownTime(float value)
        {
            Timer += value;
            TimeToWait += value;
        }

        public int GetSpawnedEnemiesCount()
        {
            return SpawnedEnemiesCount;
        }

        private void Awake()
        {
            ResourceManager = CompositionRoot.GetResourceManager();
        }

        private void FixedUpdate()
        {
            Timer -= Time.fixedDeltaTime;
            TimerProgress(Timer/TimeToWait);
            if (Timer <= 0)
            {
                Spawn();
                TimeToWait = SpawnTime;
                Timer = SpawnTime;
            }
        }

        private void Spawn()
        {
            Enemies enemyType = (Enemies)UnityEngine.Random.Range(0, 3);  //random enemy
            var instance = ResourceManager.GetFromPool(enemyType);
            var enemy = instance.GetComponent<BaseEnemy>();
            enemy.Position = new Vector3(UnityEngine.Random.Range(-16.5f, 16.5f), 1f, UnityEngine.Random.Range(-8.5f, 6.5f));

            SpawnedEnemiesCount++;
        }
    }
}