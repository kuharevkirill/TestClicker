using System;
using System.Collections.Generic;

namespace ClickerGame
{
    public interface IEnemyRepository
    {
        event Action<int> MonsterAdded;
        event Action<int, IEnemy> MonsterKilled;

        List<IEnemy> GetEnemyList();

        void Register(IEnemy enemy);
        void Unregister(IEnemy enemy);
    }
}