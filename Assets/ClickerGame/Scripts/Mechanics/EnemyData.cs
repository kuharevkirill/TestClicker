using UnityEngine;

namespace ClickerGame
{
    [CreateAssetMenu(fileName = "EnemyData", menuName = "New EnemyData")]
    public class EnemyData : ScriptableObject
    {
        public int BaseMaxHP; // start HP
        public int HPGrowthRate; // +1HP per HPGrowthRate of spawned enemies
        //public float BaseSpeed; // start speed
        //public float SpeedGrowth; // delta of speed growth
        //public float SpeedGrowthRate; // rate of speed growth
    }
}
