using UnityEngine;
using UnityEngine.EventSystems;

namespace ClickerGame
{
    public abstract class BaseBooster : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private GameObject DeathParticles;

        private IBoosterSpawner Spawner;

        public Vector3 Position
        {
            get { return transform.position; }
            set { transform.position = value; }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Collect();
            gameObject.SetActive(false);
        }

        protected virtual void Awake()
        {
            Spawner = CompositionRoot.GetBoosterSpawner();
        }

        private void OnEnable()
        {
            Spawner.Register();
        }

        private void OnDisable()
        {
            Spawner.Unregister();
        }

        protected virtual void Collect()
        {
            var particles = Instantiate(DeathParticles);
            particles.transform.position = transform.position;
        }
    }
}