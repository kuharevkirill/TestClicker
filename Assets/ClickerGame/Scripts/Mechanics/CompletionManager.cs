using System;
using UnityEngine;

namespace ClickerGame
{
    public class CompletionManager : MonoBehaviour, ICompletionManager
    {
        public event Action<int> GameOver = value => { };
        public event Action<int> ScoreChanged = value => { };

        const int LosingAmount = 10;
        private int KilledEnemies = 0;

        private void Awake()
        {
            var enemyRepository = CompositionRoot.GetEnemyRepository();
            enemyRepository.MonsterAdded += OnMonsterAdded;
            enemyRepository.MonsterKilled += OnMonsterKilled;
        }

        private void Start()
        {
            var scoreManager = CompositionRoot.GetScoreManager();
            GameOver += scoreManager.UpdateScore;
        }

        private void OnMonsterAdded(int amount)
        {
            if (amount >= LosingAmount)
            {
                GameOver(KilledEnemies);
            }
        }

        private void OnMonsterKilled(int amount, IEnemy enemy)
        {
            KilledEnemies++;
            ScoreChanged(KilledEnemies);
            // simply counts killed enemies
            // also can use some enemy cost in score, if we have that concept in work
        }
    }
}