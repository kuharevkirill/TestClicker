using UnityEngine;
using UnityEngine.EventSystems;

namespace ClickerGame
{
    public interface IEnemy : IPointerClickHandler
    {
        Vector3 Position { get; set; }
        void HandleClick(int damage);
    }
}