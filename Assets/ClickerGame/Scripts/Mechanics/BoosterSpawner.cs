using System;
using UnityEngine;

namespace ClickerGame
{
    public class BoosterSpawner : MonoBehaviour, IBoosterSpawner
    {
        [SerializeField]
        private float SpawnTime = 7f;
        [SerializeField]
        private int MaxAmount = 2;

        private int CurrentAmount = 0;
        private float Timer = 0f;

        private IResourceManager ResourceManager;

        public void Register()
        {
            CurrentAmount++;
        }

        public void Unregister()
        {
            CurrentAmount--;
        }

        private void Awake()
        {
            ResourceManager = CompositionRoot.GetResourceManager();
        }

        private void FixedUpdate()
        {
            if (CurrentAmount < MaxAmount) RunTimer();
        }

        private void RunTimer()
        {
            Timer -= Time.fixedDeltaTime;
            if (Timer <= 0)
            {
                Timer = SpawnTime;
                Spawn();
            }
        } 

        private void Spawn()
        {
            Boosters boosterType = (Boosters)UnityEngine.Random.Range(0, 2);
            var instance = ResourceManager.GetFromPool(boosterType);
            var booster = instance.GetComponent<BaseBooster>();
            booster.Position = new Vector3(UnityEngine.Random.Range(-16.5f, 16.5f), 1f, UnityEngine.Random.Range(-8.5f, 6.5f));
        }
    }
}