
namespace ClickerGame
{
    public interface IBoosterSpawner
    {
        void Register();
        void Unregister();
    }
}