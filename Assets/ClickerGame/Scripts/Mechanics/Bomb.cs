using UnityEngine;
using UnityEngine.EventSystems;

namespace ClickerGame
{
    public class Bomb : BaseBooster
    {
        [SerializeField]
        private int Damage = 1;

        private IEnemyRepository EnemyRepository;

        protected override void Awake()
        {
            base.Awake();
            EnemyRepository = CompositionRoot.GetEnemyRepository();
        }

        protected override void Collect()
        {
            base.Collect();
            var list = EnemyRepository.GetEnemyList();

            for (var i = list.Count - 1; i >= 0; i--)
            {
                var enemy = list[i];
                enemy.HandleClick(Damage);
            }
        }
    }
}