using System;

namespace ClickerGame
{
    public interface ICompletionManager
    {
        event Action<int> GameOver;
        event Action<int> ScoreChanged;
    }
}