using UnityEngine;
using UnityEngine.UI;

namespace ClickerGame
{
    public class HUD : BaseView
    {
        public Button PauseButton;
        public Text ScoreText;
        public GameObject[] Skulls = new GameObject[10];
        public Image SpawnCoolDownBar;

        private BaseView PauseLink;
        private bool IsPaused = false;

        public void SetPauseLink(BaseView link)
        {
            PauseLink = link;
        }

        public void UpdateBarFill(float fill)
        {
            SpawnCoolDownBar.fillAmount = fill;
        }

        public void UpdateScoreText(int score)
        {
            if (ScoreText != null)
            {
                ScoreText.text = $"SCORE : {score}";
            }
        }

        public void ChangeSkullAmount(int amount)
        {
            for (int i = 0; i < 10; i++)
            {
                if (Skulls[i] == null) return;

                if ((i+1) <= amount)
                {
                    Skulls[i].SetActive(true);
                }
                else
                {
                    Skulls[i].SetActive(false);
                }
            }
        }

        private void Awake()
        {
            PauseButton.onClick.AddListener(OnPause);
        }

        private void OnPause()
        {
            if (!IsPaused)
            {
                PauseLink.Show();
                IsPaused = true;
                return;
            }

            if (IsPaused)
            {
                PauseLink.Hide();
                IsPaused = false;
            }
        }
    }
}