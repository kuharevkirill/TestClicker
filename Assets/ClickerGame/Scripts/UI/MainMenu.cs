using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace ClickerGame
{
    public class MainMenu : BaseView
    {
        public Button Play;
        public Button BestScore;
        public Button Credits;
        public Button Quit;

        private BaseView CreditsLink;
        private BaseView BestScoreLink;

        public void SetCreditsLink(BaseView link)
        {
            CreditsLink = link;
        }

        public void SetBestScoreLink(BaseView link)
        {
            BestScoreLink = link;
        }

        private void Awake()
        {
            Play.onClick.AddListener(OnPlay);
            BestScore.onClick.AddListener(OnBestScore);
            Credits.onClick.AddListener(OnCredits);
            Quit.onClick.AddListener(OnQuit);
        }

        private void OnPlay()
        {
            SceneManager.LoadScene(1);
        }

        private void OnBestScore()
        {
            BestScoreLink.Show();
            Hide();
        }

        private void OnCredits()
        {
            CreditsLink.Show();
            Hide();
        }

        private void OnQuit()
        {
            Application.Quit();
        }
    }
}