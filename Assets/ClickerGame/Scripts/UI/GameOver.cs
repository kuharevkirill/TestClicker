using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace ClickerGame
{
    public class GameOver : BaseView
    {
        public Button Proceed;
        public Text ScoreText;

        public void UpdateScoreText(int score)
        {
            ScoreText.text = $"YOUR SCORE : {score}";
        }

        public override void Show()
        {
            base.Show();
            Time.timeScale = 0f;
        }

        private void Awake()
        {
            Proceed.onClick.AddListener(OnProceed);
        }

        private void OnProceed()
        {
            // score evaluation
            Time.timeScale = 1f;
            SceneManager.LoadScene(0);
        }
    }
}