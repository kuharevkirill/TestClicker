using UnityEngine;

namespace ClickerGame
{
    public abstract class BaseView : MonoBehaviour, IScreen
    {
        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        public void SetParent(Transform parent)
        {
            transform.SetParent(parent, false);
        }
    }
}

