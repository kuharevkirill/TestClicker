using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace ClickerGame
{
    public class BestScore : BaseView
    {
        public Button Back;

        private BaseView MainMenuLink;
        private IScoreManager ScoreManager;

        [SerializeField]
        private Text[] Names = new Text[10];

        [SerializeField]
        private Text[] Scores = new Text[10];


        public void SetMainMenuLink(BaseView link)
        {
            MainMenuLink = link;
        }

        private void Awake()
        {
            Back.onClick.AddListener(OnBack);
        }

        private void Start()
        {
            ScoreManager = CompositionRoot.GetScoreManager();
            SetResults();
        }

        private void OnBack()
        {
            MainMenuLink.Show();
            Hide();
        }

        private void SetResults()
        {
            for (int i = 0; i < 10; i++)
            {
                Names[i].text = ScoreManager.GiveName(i);
                Scores[i].text = ScoreManager.GiveScore(i);
            }
        }
    }
}