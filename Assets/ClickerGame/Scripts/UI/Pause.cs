using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace ClickerGame
{
    public class Pause : BaseView
    {
        public Button Finish;
        public Button Continue;

        public override void Show()
        {
            base.Show();
            Time.timeScale = 0f;
        }

        public override void Hide()
        {
            base.Hide();
            Time.timeScale = 1f;
        }

        private void Awake()
        {
            Continue.onClick.AddListener(OnContinue);
            Finish.onClick.AddListener(OnFinish);
        }


        private void OnContinue()
        {
            Hide();
        }

        private void OnFinish()
        {
            // score evaluations
            Time.timeScale = 1f;
            SceneManager.LoadScene(0);
        }
    }
}