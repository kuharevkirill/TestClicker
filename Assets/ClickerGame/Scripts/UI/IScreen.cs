using UnityEngine;

namespace ClickerGame
{
    public interface IScreen
    {
        void Show();
        void Hide();
        void SetParent(Transform parent);
    }
}

