
namespace ClickerGame
{
    public enum UIScreens
    {
        MainMenu,
        BestScore,
        Credits,
        GameOver,
        Pause,
        HUD
    }
}