using UnityEngine;
using UnityEngine.UI;

namespace ClickerGame
{
    public class Credits : BaseView
    {
        private BaseView MainMenuLink;

        public Button Back;

        public void SetMainMenuLink(BaseView link)
        {
            MainMenuLink = link;
        }

        private void Awake()
        {
            Back.onClick.AddListener(OnBack);
        }

        private void OnBack()
        {
            MainMenuLink.Show();
            Hide();
        }
    }
}