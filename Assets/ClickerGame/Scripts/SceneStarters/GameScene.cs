using UnityEngine;

namespace ClickerGame
{
    public class GameScene : MonoBehaviour
    {
        private Pause Pause;
        private GameOver GameOver;
        private HUD HUD;

        private void Awake()
        {
            var resourceManager = CompositionRoot.GetResourceManager();
            var uiRoot = CompositionRoot.GetUIRoot();
            var enemySpawner = CompositionRoot.GetEnemySpawner();
            var enemyRepository = CompositionRoot.GetEnemyRepository();
            var completionManager = CompositionRoot.GetCompletionManager();
            var scoreManager = CompositionRoot.GetScoreManager();
            var boosterSpawner = CompositionRoot.GetBoosterSpawner();

            Pause = resourceManager.CreatePrefab<Pause, UIScreens>(UIScreens.Pause);
            GameOver = resourceManager.CreatePrefab<GameOver, UIScreens>(UIScreens.GameOver);
            HUD = resourceManager.CreatePrefab<HUD, UIScreens>(UIScreens.HUD);

            Pause.SetParent(uiRoot.MenuCanvas);
            GameOver.SetParent(uiRoot.MenuCanvas);
            HUD.SetParent(uiRoot.MainCanvas);

            HUD.SetPauseLink(Pause);

            Pause.Hide();
            GameOver.Hide();
            HUD.Show();

            completionManager.GameOver += OnGameOver;
            completionManager.ScoreChanged += OnScoreChanged;

            enemyRepository.MonsterAdded += AddSkull;
            enemyRepository.MonsterKilled += RemoveSkull;

            enemySpawner.TimerProgress += OnTimerProgress;
        }

        private void OnGameOver(int score)
        {
            GameOver.UpdateScoreText(score);
            GameOver.Show();
        }

        private void OnScoreChanged(int score)
        {
            HUD.UpdateScoreText(score);
        }

        private void AddSkull(int value)
        {
            HUD.ChangeSkullAmount(value);
        }

        private void RemoveSkull(int value, IEnemy enemy)
        {
            HUD.ChangeSkullAmount(value);
        }

        private void OnTimerProgress(float value)
        {
            HUD.UpdateBarFill(value);
        }
    }
}