using System;
using UnityEngine;
using UnityEngine.UI;

namespace ClickerGame
{
    public class MenuScene : MonoBehaviour
    {
        private MainMenu MainMenu;
        private Credits Credits;
        private BestScore BestScore;

        private void Awake()
        {
            var resourceManager = CompositionRoot.GetResourceManager();
            var scoreManager = CompositionRoot.GetScoreManager();
            var uiRoot = CompositionRoot.GetUIRoot();

            MainMenu = resourceManager.CreatePrefab<MainMenu, UIScreens>(UIScreens.MainMenu);
            Credits = resourceManager.CreatePrefab<Credits, UIScreens>(UIScreens.Credits);
            BestScore = resourceManager.CreatePrefab<BestScore, UIScreens>(UIScreens.BestScore);

            MainMenu.SetParent(uiRoot.MenuCanvas);
            Credits.SetParent(uiRoot.MenuCanvas);
            BestScore.SetParent(uiRoot.MenuCanvas);

            MainMenu.SetCreditsLink(Credits);
            MainMenu.SetBestScoreLink(BestScore);
            Credits.SetMainMenuLink(MainMenu);
            BestScore.SetMainMenuLink(MainMenu);

            MainMenu.Show();
            Credits.Hide();
            BestScore.Hide();
        }
    }
}

