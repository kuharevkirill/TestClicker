using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClickerGame
{
    public class UIRoot : MonoBehaviour, IUIRoot
    {
        public Transform MenuCanvasLink;
        public Transform MainCanvasLink;

        public Transform MenuCanvas => MenuCanvasLink;
        public Transform MainCanvas => MainCanvasLink;
    }
}