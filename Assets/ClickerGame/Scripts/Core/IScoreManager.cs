using UnityEngine;

namespace ClickerGame
{
    public interface IScoreManager
    {
        void UpdateScore(int value);
        string GiveName(int number);
        string GiveScore(int number);
    }
}