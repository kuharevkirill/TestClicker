using UnityEngine;

namespace ClickerGame
{
    public class CompositionRoot : MonoBehaviour
    {
        private static IResourceManager ResourceManager;
        private static ICompletionManager CompletionManager;
        private static IEnemyRepository EnemyRepository;
        private static IEnemySpawner EnemySpawner;
        private static IScoreManager ScoreManager;
        private static IUIRoot UIRoot;

        private static IBoosterSpawner BoosterSpawner;

        public static IResourceManager GetResourceManager()
        {
            if (ResourceManager == null)
            {
                ResourceManager = new ResourceManager();
            }

            return ResourceManager;
        }

        public static IScoreManager GetScoreManager() 
        {
            if (ScoreManager == null)
            {
                ScoreManager = new ScoreManager();
            }

            return ScoreManager;
        }

        public static IUIRoot GetUIRoot()
        {
            if (UIRoot == null)
            {
                var resourceManager = GetResourceManager();
                UIRoot = resourceManager.CreatePrefab<IUIRoot, Components>(Components.UIRoot);
            }

            return UIRoot;
        }

        public static IEnemySpawner GetEnemySpawner()
        {
            if (EnemySpawner == null)
            {
                var resourceManager = GetResourceManager();
                EnemySpawner = resourceManager.CreatePrefab<IEnemySpawner, Components>(Components.EnemySpawner);
            }

            return EnemySpawner;
        }

        public static ICompletionManager GetCompletionManager()
        {
            if (CompletionManager == null)
            {
                var resourceManager = GetResourceManager();
                CompletionManager = resourceManager.CreatePrefab<ICompletionManager, Components>(Components.CompletionManager);
            }

            return CompletionManager;
        }

        public static IEnemyRepository GetEnemyRepository()
        {
            if (EnemyRepository == null)
            {
                var resourceManager = GetResourceManager();
                EnemyRepository = resourceManager.CreatePrefab<IEnemyRepository, Components>(Components.EnemyRepository);
            }

            return EnemyRepository;
        }

        public static IBoosterSpawner GetBoosterSpawner()
        {
            if (BoosterSpawner == null)
            {
                var resourceManager = GetResourceManager();
                BoosterSpawner = resourceManager.CreatePrefab<IBoosterSpawner, Components>(Components.BoosterSpawner);
            }
            return BoosterSpawner;
        }

        private void OnDestroy()
        {
            UIRoot = null;
            EnemySpawner = null;
            BoosterSpawner = null;
            EnemyRepository = null;
            CompletionManager = null;
            ResourceManager = null; // it has object pool
        }
    }
}

