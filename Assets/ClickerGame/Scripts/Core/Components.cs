
namespace ClickerGame
{
    public enum Components
    {
        UIRoot,
        EnemySpawner,
        CompletionManager,
        EnemyRepository,
        BoosterSpawner
    }
}

