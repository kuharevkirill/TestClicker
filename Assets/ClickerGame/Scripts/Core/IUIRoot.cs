using UnityEngine;

namespace ClickerGame
{
    public interface IUIRoot
    {
        Transform MenuCanvas { get; }
        Transform MainCanvas { get; }
    }
}

