using System;
using UnityEngine;

namespace ClickerGame
{
    public struct PoolItem
    {
        public Type type;
        public Enum value;
        public GameObject item;
    }
}