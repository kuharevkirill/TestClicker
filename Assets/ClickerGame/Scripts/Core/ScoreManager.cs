using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ClickerGame
{
    public class ScoreManager : IScoreManager
    {
        private int LastScore = 0;

        private static List<int> Scores = new List<int> { 18, 21, 24, 32, 39, 50, 60, 72, 85, 100 };
        private static Dictionary<int, string> Winners = new Dictionary<int, string>()
        {
            { Scores[0], "Noob"},
            { Scores[1], "Rookie"},
            { Scores[2], "Dyatel"},
            { Scores[3], "Medved"},
            { Scores[4], "Stinky"},
            { Scores[5], "Arthur"},
            { Scores[6], "CoolGirl"},
            { Scores[7], "Botan"},
            { Scores[8], "Negodnik"},
            { Scores[9], "Popados"}
        };

        public void UpdateScore(int value)
        {
            LastScore = value;
            UpdateWinnerScores();
            UpdateWinnerNames();
        }

        public string GiveName(int number)
        {
            return Winners[Scores[number]];
        }

        public string GiveScore(int number)
        {
            return Scores[number].ToString();
        }

        private void UpdateWinnerNames()
        {
            foreach (int score in Scores)
            {
                if (score == LastScore)
                {
                    Winners[score] = "Player";
                }
            }
        }

        private void UpdateWinnerScores()
        {
            Scores.Add(LastScore);
            Scores.Sort();
            Scores.RemoveAt(0);
        }

    }
}